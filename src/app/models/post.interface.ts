//Permet de décrire les données

export interface Post {
    id : number;
    title: string,
    content: string,
    loveIts: number,
    created_at?: Date
  } 