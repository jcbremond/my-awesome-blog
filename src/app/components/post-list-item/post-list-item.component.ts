import { Component, OnInit, Input } from '@angular/core';
import { Post } from 'src/app/models/post.interface';

@Component({
  selector: 'post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  //@Input() post_id: number;
  //@Input() post_title: string;
  //@Input() post_content: string;
  //@Input() post_loveIts: number;
  //@Input() post_created_at: Date;

  @Input() post_full:Post;

   //title_interne: string = "Titre";
   //content_interne: string = "Contenu";
   //like_interne: number = 7;

  constructor() { }

  ngOnInit() {
  }

  onLike() {
    console.log("J'aime");
    if (this.post_full.loveIts==-1){
      this.post_full.loveIts=0;
    }

    this.post_full.loveIts++;
  }

  onDislike() {
    console.log("J'aime pas");
    if (this.post_full.loveIts==1){
      this.post_full.loveIts=0;
    }
    this.post_full.loveIts--;
  }

  getLikeNumber(){
   return this.post_full.loveIts; 
  }

  getColor() {
    if(this.post_full.loveIts === 0) {
      return 'black';
    } else if(this.post_full.loveIts < 0 ) {
      return 'red';
    }else if(this.post_full.loveIts > 0 ) {
      return 'green';
    }
}
}
