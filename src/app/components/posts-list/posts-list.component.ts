import { Component, OnInit } from '@angular/core';

import {Post} from '../../models/post.interface';
import {PostsService} from '../../services/posts.service' ;

@Component({
  selector: 'posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss']
})
export class PostsListComponent implements OnInit {

  constructor(private loader: PostsService) {}

  // Tableau qui va contenir les posts
  // Ils sont typés grâce à l'interface qui définit la structure de l'objet.
  posts: Post[];

  ngOnInit() {
    // Récupération de la liste des posts depuis le service
    this.posts = this.loader.getPosts(); 
  }

}
