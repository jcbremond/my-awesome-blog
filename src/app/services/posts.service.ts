import { Injectable } from '@angular/core';

import {Post} from '../models/post.interface';

@Injectable({
  providedIn: 'root'
})
export class PostsService {


  posts: Post[] = [
    { id: 1, title: "Mon premier post",  content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nunc dolor, interdum in finibus quis, iaculis eget ligula. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce pulvinar lectus volutpat leo laoreet auctor. Praesent consectetur arcu ac felis tempor, eget sodales erat pulvinar. Donec commodo enim sed magna mollis, at lobortis dolor rutrum. Aliquam fringilla lorem a purus tristique mattis. Praesent sed lectus ornare, iaculis nisi ac, aliquet magna.",loveIts :6,created_at : new Date() },
    { id: 2, title: "Post n°2",          content: "Vestibulum mollis risus id pretium varius. Nulla sagittis gravida odio sit amet pretium. Sed pretium risus mi, eget volutpat justo consequat vitae. Suspendisse nec diam dolor. Praesent vulputate facilisis turpis nec porttitor. Nunc viverra rhoncus eleifend. Proin diam velit, elementum nec ligula ut, molestie viverra velit. Curabitur dignissim, sem ut tempus congue, tortor eros dictum nibh, quis accumsan ex mi sollicitudin nunc. Praesent non est at diam euismod ultrices. Nullam condimentum dignissim est. Pellentesque interdum nisi sit amet massa condimentum sodales.",loveIts :-1,created_at : new Date() },
    { id: 3, title: "Un troisième post.",content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et felis vitae velit tincidunt feugiat. Duis neque mauris, aliquet eu.",loveIts :0,created_at : new Date() },
    //{ id: 4, title: "titre n°4",content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et felis vitae velit tincidunt feugiat. Duis neque mauris, aliquet eu.",loveIts :5,created_at : new Date() }
   ];

  constructor() { }

  getPosts(): Post[]{
    return this.posts;
  }

}
